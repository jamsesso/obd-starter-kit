# OBD Starter Kit

This repository is meant to act as a starting point for anyone who attended my talk on January 30, 2017 at the University of New Brunswick's CS Square. It comes with Gradle pre-configured with the [Autobit OBD](https://bitbucket.org/autobit/obd) dependency.

### Cloning the Repository

```sh
git clone ﻿git@bitbucket.org:jamsesso/obd-starter-kit.git
cd obd-starter-kit
```

### Building the Project

```sh
./gradlew build
```

### Running the Project

```sh
./gradlew run
```
