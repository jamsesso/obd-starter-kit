import ca.autobit.obd.Connector;
import ca.autobit.obd.SerialConnector;
import ca.autobit.obd.request.BatteryVoltageRequest;
import ca.autobit.obd.request.EngineRPMRequest;
import ca.autobit.obd.request.VehicleSpeedRequest;

import java.io.IOException;

public class Application {
  public static void main(String[] args) {
    try(Connector connector = new SerialConnector("/dev/ttyUSB0")) {
      connector.connect();

      // Create request objects
      VehicleSpeedRequest speedRequest = new VehicleSpeedRequest();
      EngineRPMRequest rpmRequest = new EngineRPMRequest();
      BatteryVoltageRequest voltageRequest = new BatteryVoltageRequest();

      // Get data
      while(true) {
        Integer speed = connector.request(speedRequest);
        Double rpm = connector.request(rpmRequest);
        Double voltage = connector.request(voltageRequest);

        System.out.print("\rSpeed = " + String.format("%8s km/h", speed) + "\t" +
                         "RPM = " + String.format("%8s rpm", rpm) + "\t" +
                         "Voltage = " + String.format("%8sV", voltage));
      }
    }
    catch(IOException e) {
      e.printStackTrace();
    }
  }
}
